$(function(){
	console.log('executing......');

	$("#searchBox").keyup((event) => {
		console.log(event, $("#searchBox").val());
	});

	$("#imageUploadBtn").click(() => {
		console.log('clicked imageUploadBtn');
	});

	$("#imageTitleAdd").click(() => {
		var imageTitle = $("#imageTitle").val();
		var f= $('.dz-details').find('span').eq(1).text();
		console.log('imageTitleAdd=>', imageTitle);
		console.log('imageTitleAdd=>f=>', f);
	});

/*
	// preventing page from redirecting
    $("html").on("dragover", (e) => {
        e.preventDefault();
        e.stopPropagation();
        $("h1").text("Drag here");
    });

    $("html").on("drop",  (e) => { 
    	e.preventDefault(); e.stopPropagation(); 
    });

    // Drag enter
    $('.upload-area').on('dragenter', (e) => {
        e.stopPropagation();
        e.preventDefault();
        $("h1").text("Drop");
    });

    // Drag over
    $('.upload-area').on('dragover', (e) => {
        e.stopPropagation();
        e.preventDefault();
        $("h1").text("Drop");
    });

    // Drop
    $('.upload-area').on('drop',  (e)=> {
        e.stopPropagation();
        e.preventDefault();

        $("h1").text("Upload");
        var file = e.originalEvent.dataTransfer.files;
        var fd = new FormData();
        fd.append('file', file[0]);
        uploadData(fd);
    });

    // Open file selector on div click
    $("#uploadfile").click(() => {
        $("#file").click();
    });

    // file selected
    $("#file").change(() => {        
        var files = $('input[type=file]')[0].files[0];
        console.log('fileinfo=>', files);
        uploadData(files);
    });

	// Sending AJAX request and upload file
	function uploadData(formData) {
		 var fd = new FormData();
        fd.append('file',formData);
	    console.log('uploadData=>', formData);
	    console.log('uploadData=>fd=>', fd);
	    console.log('lost=>',$('meta[name="csrf-token"]').attr('content'));
	    $.ajax({
	        url: '/image_uploader/upload-target',
	        type: 'post',
	        headers: {
	        	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    	},
	        data: formData,
			contentType: false,
			cache: false,
			processData: false,
	        dataType: 'json',
	        success: (response) =>{
	            addThumbnail(response);
	        }
	    });
	}

	// Added thumbnail
	function  addThumbnail (data)  {
	    $("#uploadfile h1").remove(); 
	    var len = $("#uploadfile div.thumbnail").length;
	    var num = Number(len);
	    num = num + 1;
	    var name = data.name;
	    var size = convertSize(data.size);
	    var src = data.src;

	    // Creating an thumbnail
	    $("#uploadfile").append('<div id="thumbnail_'+num+'" class="thumbnail"></div>');
	    $("#thumbnail_"+num).append('<img src="'+src+'" width="100%" height="78%">');
	    $("#thumbnail_"+num).append('<span class="size">'+size+'<span>');
	}

	// Bytes conversion
	function convertSize(size)  {
	    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
	    if (size == 0) return '0 Byte';
	    var i = parseInt(Math.floor(Math.log(size) / Math.log(1024)));
	    return Math.round(size / Math.pow(1024, i), 2) + ' ' + sizes[i];
	}
*/

 });