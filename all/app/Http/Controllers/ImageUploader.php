<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ImageUploader extends Controller
{
    public function home() {
    	return view('image-upload');
    }

	public function uploadImages(Request $request) {		
        $image = $request->file('file');
        $profileImage = $image->getClientOriginalName();
        // Define upload path
        $destinationPath = public_path('/images/'); // upload path
        $image->move($destinationPath,$profileImage);       
        return response()->json(["success"=>$image]);
    }

    public function homeDeleteUpload(Request $request)  {
    	 $image = $request->file('filename');
        $filename =  $request->get('filename');
        
        $path = public_path().'/images/'.$filename;
        if (file_exists($path)) {
            unlink($path);
        }
        return $filename;
    }
}
