<?php

use Illuminate\Support\Facades\Route;



// Route::get('/', function () {     return view('welcome'); });


Route::get('/', 'ImageUploader@home');
Route::get('/home', 'ImageUploader@home');


Route::post('/store', 'ImageUploader@uploadImages');

Route::post('/upload-target', 'ImageUploader@homeUpload');
Route::post('/dropzone-image-delete', 'ImageUploader@homeDeleteUpload');